# React - The complete Guide

## Section 4 - Lists & Conditionals, homework solution

The instructions are:

- Create an input field (in App component) with a change listener which outputs the length of the entered text below it (e.g. in a paragraph).
- Create a new component (=> ValidationComponent) which receives the text length as a prop
- Inside the ValidationComponent, either output "Text too short" or "Text long enough" depending on the text length (e.g. take 5 as a minimum length)
- Create another component (=> CharComponent) and style it as an inline box (=> display: inline-block, padding: 16px, text-align: center, margin: 16px, border: 1px solid black).
- Render a list of CharComponents where each CharComponent receives a different letter of the entered text (in the initial input field) as a prop.
- When you click a CharComponent, it should be removed from the entered text.

## Author

Ariel Duarte

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
