import React, { useState } from 'react';
import './App.css';

import Validation from './Components/Validation';
import Char from './Components/Char';
const App = () => {
  const [state, setState] = useState({ name: 'TEST' });

  const inputChangeHandler = e => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    });
  };

  const removeCharHandler = i => {
    const name = state.name.split('');
    name.splice(i, 1);
    setState({ name: name.join('') });
  };

  const charList = state.name.split('').map((c, index) => {
    return (
      <Char
        character={c}
        key={index}
        clicked={() => removeCharHandler(index)}
      />
    );
  });

  return (
    <div className="App">
      <h2> Section 4 - Lists & Conditionals, homework solution</h2>
      <input
        type="text"
        name="name"
        onChange={inputChangeHandler}
        value={state.name}
      />
      <p>{state.name}</p>
      <Validation textLength={state.name.length} />
      {charList}
      <hr />

      <ol>
        <li>
          Create an input field (in App component) with a change listener which
          outputs the length of the entered text below it (e.g. in a paragraph).
        </li>
        <li>
          Create a new component (=> ValidationComponent) which receives the
          text length as a prop
        </li>
        <li>
          Inside the ValidationComponent, either output "Text too short" or
          "Text long enough" depending on the text length (e.g. take 5 as a
          minimum length)
        </li>
        <li>
          Create another component (=> CharComponent) and style it as an inline
          box (=> display: inline-block, padding: 16px, text-align: center,
          margin: 16px, border: 1px solid black).
        </li>
        <li>
          Render a list of CharComponents where each CharComponent receives a
          different letter of the entered text (in the initial input field) as a
          prop.
        </li>
        <li>
          When you click a CharComponent, it should be removed from the entered
          text.
        </li>
      </ol>
      <p>Hint: Keep in mind that JavaScript strings are basically arrays!</p>
    </div>
  );
};

export default App;
