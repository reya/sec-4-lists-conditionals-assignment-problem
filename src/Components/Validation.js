import React from 'react';

const Validation = ({ textLength }) => {
  const validationText = l => {
    const message = l <= 5 ? 'Text too short' : 'Text long enough';
    return (
      <div>
        <p>{message}</p>
      </div>
    );
  };

  return (
    <div>
      <h4>Validation Component</h4>
      {validationText(textLength)}
    </div>
  );
};

export default Validation;
