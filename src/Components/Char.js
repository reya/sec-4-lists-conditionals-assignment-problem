import React from 'react';

const Char = ({ character, clicked }) => {
  const style = {
    display: 'inline-block',
    padding: '16px',
    textAlign: 'center',
    margin: '16px',
    border: '1px solid black',
    cursor: 'pointer'
  };

  return (
    <div style={style} onClick={clicked}>
      {character}
    </div>
  );
};

export default Char;
